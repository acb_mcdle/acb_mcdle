import java.util.Scanner;
public class M5_p4 {
    private Scanner sc;
    public M5_p4() {
        this.sc = new Scanner(System.in);
    }
    public int readUserInput() {
        return sc.nextInt();
    }
    public void end() {
        System.out.println("FI");
    }
    public void showMenu() {
        System.out.println("Operacions:");
        System.out.print("Escull una opció:");
        System.out.println(" ");
        System.out.println("0.Sortir del programa" );
    }
    public void operate(int opcio) {
        switch (opcio) {
            case 0:
                end();
                break;
        }
    }
    public void main(String[] args) {
        M5_p4 p4 = new M5_p4();
        int response;
        do {
            p4.showMenu();
            response = p4.readUserInput();
            p4.operate(response);
        } while (response != 0);
    }
}
